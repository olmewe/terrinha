﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Intro:MonoBehaviour {
		Renderer bg,bg2;
		Transform click;
		AudioSource aud;

		void Awake() {
			Game.Verify();
			Game.mainCam = transform.Find("cam").GetComponent<Camera>();
			bg = transform.Find("bg").GetComponent<Renderer>();
			bg2 = transform.Find("bg2").GetComponent<Renderer>();
			bg2.material.color = new Color(1,1,1,.1f);
			bg.material.mainTextureScale = new Vector2(1.5f,1.5f);
			bg2.material.mainTextureScale = new Vector2(1.5f,1.5f);
			click = transform.Find("click!!");
			aud = GetComponent<AudioSource>();
			Cursor.visible = true;
		}

		void Update() {
			bg.material.mainTextureOffset = new Vector2(Time.time*.01f,0);
			bg2.material.mainTextureOffset = new Vector2(Time.time*.02f,0);
			click.localScale = Vector3.one*(1+Mathf.Sin(Time.time*2)*.05f);
			if (Input.mouseRelease) {
				aud.PlayOneShot(aud.clip);
				Game.ChangeScene(1);
			}
		}
	}
}