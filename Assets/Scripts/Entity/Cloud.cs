﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Cloud:MonoBehaviour {
		public Entity entity { get; private set; }

		Renderer rend;
		ParticleSystem particles;
		Transform click;
		AudioSource aud;

		float alpha = 0;
		float tempo;

		void Awake() {
			entity = GetComponent<Entity>();
			entity.dir = 1;
			rend = transform.Find("sprite").GetComponent<Renderer>();
			particles = transform.Find("particles").GetComponent<ParticleSystem>();
			click = transform.Find("click");
			tempo = Random.Range(60,2*60);
			aud = GetComponent<AudioSource>();
			aud.Play();
			SetAlpha();
		}

		void Update() {
			if (tempo > 0) {
				tempo -= Time.deltaTime;
				if (tempo <= 0) {
					tempo = 0;
					particles.Stop();
				}
			}
			if (tempo > 0) {
				if (alpha < 1) {
					alpha += Time.deltaTime;
					if (alpha >= 1) {
						particles.Play();
						alpha = 1;
					}
					SetAlpha();
				}
			} else {
				if (alpha > 0) {
					alpha -= Time.deltaTime*.3f;
					if (alpha <= 0) {
						alpha = 0;
						GetComponent<Spawnable>().Destroy();
					}
					SetAlpha();
				}
			}
			const float r = 2.25f;
			bool doStuff = alpha > .5f;
			float d = entity.GetPlayerUnitDistance();
			if (Mathf.Abs(d) < r) {
				Level.me.hud.DrinkFromRain(alpha);
			}
			if (doStuff) {
				if (Level.me.DetectPlayerClick(click.position,3) && Level.me.hud.FillBucket()) {
					Level.me.ConsumePlayerClick();
					Level.me.ActivatePlayerCooldown();
				}
				const float r2 = r*.6f;
				int s = Level.GetFixedUnit(entity.angle-r2/Level.worldRadius);
				int e = Level.GetFixedUnit(entity.angle+r2/Level.worldRadius);
				bool turn = s > e;
				while (true) {
					if (s >= Level.fixedLength) {
						if (!turn) break;
						s -= Level.fixedLength;
						turn = false;
					}
					if (!turn && s > e) break;
					if (Level.me.fixedEntities[s] != null) {
						switch (Level.me.fixedEntities[s].type) {
							case FixedEntityType.Campfire:
								Level.me.fixedEntities[s].Destroy();
								break;
							case FixedEntityType.Plantation:
								Level.me.fixedEntities[s].GetComponent<Plantation>().Water();
								break;
						}
					}
					s++;
				}
			}
		}

		void SetAlpha() {
			rend.material.color = new Color(1,1,1,Tween.EaseOut(alpha));
			aud.volume = alpha;
		}
	}
}