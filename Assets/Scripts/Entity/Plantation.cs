﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Plantation:FixedEntity {
		Renderer rend;
		Renderer carrot;
		AudioSource aud;

		public float growTempo;
		public int grownState;

		public AudioClip sfxAction,sfxWater;

		float water = 0;

		void Awake() {
			entity = GetComponent<Entity>();
			rend = transform.Find("sprite").GetComponent<Renderer>();
			carrot = transform.Find("carrot").GetComponent<Renderer>();
			growTempo = Level.dayCycleDuration/2f;
			grownState = 0;
			aud = GetComponent<AudioSource>();
		}

		void Start() {
			UpdateSprite();
		}

		void Update() {
			if (grownState == 1) {
				if (water > 0) {
					water -= Time.deltaTime/Level.dayCycleDuration;
					if (water < 0) water = 0;
				}
				growTempo -= Time.deltaTime*water;
				if (growTempo <= 0) {
					growTempo = 0;
					grownState = 2;
				}
			}
			UpdateSprite();
			if (Level.me.DetectPlayerClick(entity.tr.position,.5f)) {
				if (!Level.me.hud.InteractWithPlantation(this)) {
					Level.me.ConsumePlayerClick();
					Level.me.ActivatePlayerCooldown();
					Hit(.5f,0);
				}
			}
		}

		void UpdateSprite() {
			int p;
			p = fixedPosition-1;
			if (p < 0) p += Level.fixedLength;
			bool right = Level.me.fixedEntities[p] != null && Level.me.fixedEntities[p].type == FixedEntityType.Plantation;
			p = fixedPosition+1;
			if (p >= Level.fixedLength) p -= Level.fixedLength;
			bool left = Level.me.fixedEntities[p] != null && Level.me.fixedEntities[p].type == FixedEntityType.Plantation;
			float x;
			if (left) {
				x = right ? .5f : .75f;
			} else {
				x = right ? .25f : 0;
			}
			float y = (grownState > 0) ? .25f : 0;
			if (water >= .5f) y += .5f;
			rend.material.mainTextureOffset = new Vector2(x,y);
			carrot.material.mainTextureOffset = new Vector2(0,.9f-grownState*.3f);
		}

		public bool Water() {
			if (grownState < 2) {
				water = 2;
				return true;
			}
			return false;
		}

		public bool PlantSeed() {
			if (grownState < 1) {
				grownState = 1;
				aud.PlayOneShot(sfxAction);
				return true;
			}
			return false;
		}

		public void BadSpider() {
			grownState = 0;
			water = 0;
		}

		public void PlaySfxWater() {
			aud.PlayOneShot(sfxWater);
		}

		public override bool Hit(float damage,float otherAngle) {
			if (damage < .75f) {
				if (grownState < 2) {
					Level.me.hud.GetItem(ItemType.CarrotSeed);
				} else {
					Level.me.hud.GetItem(ItemType.Carrot);
					Level.me.hud.GetItem(ItemType.CarrotSeed,Random.Range(1,3));
				}
			}
			Destroy();
			return true;
		}

		protected override FixedEntityType CreateEntity() {
			return FixedEntityType.Plantation;
		}

		protected override void DestroyEntity() {
			Level.me.Dust(entity.tr.position,15);
		}
	}
}