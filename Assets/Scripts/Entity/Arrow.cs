﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Arrow:EntityBase {
		public Entity entity { get; private set; }

		Renderer rend;

		Vector2 pos;

		void Awake() {
			entity = GetComponent<Entity>();
			Level.me.arrows.Add(this);
			rend = transform.Find("sprite").GetComponent<Renderer>();
			rend.enabled = false;
		}

		void Start() {
			pos = entity.tr.localPosition;
		}

		void Update() {
			rend.enabled = true;
			var newPos = entity.tr.localPosition;
			entity.tr.localEulerAngles = new Vector3(0,0,Mathf.Atan2(newPos.y-pos.y,newPos.x-pos.x)*Mathf.Rad2Deg);
			pos = newPos;
			if (entity.grounded) Destroy();
		}

		public void Destroy() {
			Level.me.arrows.Remove(this);
			Level.me.Dust(entity.tr.position,5);
			Destroy(gameObject);
		}
	}
}