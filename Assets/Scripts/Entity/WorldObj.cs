﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class WorldObj:MonoBehaviour {
		Transform tr;

		void Awake() {
			tr = transform;
		}
		
		void Update() {
			tr.localEulerAngles = new Vector3(0,0,Mathf.Atan2(-tr.localPosition.x,tr.localPosition.y)*Mathf.Rad2Deg);
		}
	}
}