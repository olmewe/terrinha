﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class TreeObj:FixedEntity {
		Transform sprite;
		Renderer rend;
		AudioSource aud;

		public float growTempo;
		public bool grown;

		float shake = 0;

		void Awake() {
			entity = GetComponent<Entity>();
			sprite = transform.Find("sprite");
			rend = sprite.GetComponent<Renderer>();
			grown = false;
			growTempo = Level.dayCycleDuration+Random.Range(-10f,10f);
			aud = GetComponent<AudioSource>();
		}

		void Start() {
			UpdateSprite();
		}

		void Update() {
			if (!grown) {
				growTempo -= Time.deltaTime;
				if (growTempo <= 0) {
					growTempo = 0;
					grown = true;
					UpdateSprite();
				}
			}
			if (Level.me.DetectPlayerClick(entity.tr.position,grown ? 1 : .5f)) {
				if (Level.me.hud.selectedItem == null || Level.me.hud.selectedItem.type != ItemType.Sapling) {
					Level.me.ConsumePlayerClick();
					Level.me.ActivatePlayerCooldown();
					Hit(Level.me.GetPlayerTreeChopDamage(),0);
				}
			}
			if (shake > 0) {
				shake -= Time.deltaTime*4;
				if (shake < 0) shake = 0;
				var p = sprite.localPosition;
				sprite.localPosition = new Vector3(Mathf.Sin(shake*shake*12)*.1f,p.y,p.z);
			}
		}

		void UpdateSprite() {
			rend.material.mainTextureOffset = new Vector2(grown ? .5f : 0,0);
		}

		public override bool Hit(float damage,float otherAngle) {
			aud.PlayOneShot(aud.clip);
			if (!grown) {
				Level.me.hud.GetItem(ItemType.Sapling);
				Destroy();
				return true;
			}
			if (base.Hit(damage,otherAngle)) {
				Level.me.hud.GetItem(ItemType.Wood,Random.Range(5,11));
				Level.me.hud.GetItem(ItemType.Sapling,Random.Range(2,4));
				Destroy();
				return true;
			}
			shake = 1;
			return false;
		}

		protected override FixedEntityType CreateEntity() {
			return FixedEntityType.Tree;
		}

		protected override void DestroyEntity() {
			Level.me.Dust(entity.tr.position,grown ? 40 : 10);
		}
	}
}