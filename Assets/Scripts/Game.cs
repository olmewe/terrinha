﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Terrinha {
	public class Game:MonoBehaviour {
		public static Game me { get; private set; }
		public static Camera mainCam;

		public int goToScene { get; private set; }
		public float sceneTempo { get; private set; }

		Transform tr;
		Camera bg;
		Renderer fade;

		public static void Verify() {
			if (me == null) {
				me = Instantiate(Resources.Load<GameObject>("prefabs/game")).GetComponent<Game>();
			}
		}
		
		void Awake() {
			if (me != null) {
				Destroy(gameObject);
				return;
			}
			DontDestroyOnLoad(gameObject);
			me = this;

			tr = transform;
			bg = tr.Find("bg").GetComponent<Camera>();
			fade = tr.Find("fade").GetComponent<Renderer>();

			goToScene = -1;
			sceneTempo = 0;

			LateUpdate();
			Window.Start();
		}

		void Update() {
			if (goToScene >= 0) {
				sceneTempo -= Time.deltaTime;
				if (sceneTempo <= 0) {
					sceneTempo = 0;
					int s = goToScene;
					goToScene = -1;
					SceneManager.LoadScene(s);
				}
			} else if (sceneTempo < 1) {
				sceneTempo += Time.deltaTime;
				if (sceneTempo > 1) sceneTempo = 1;
			}
			
			Window.Update();
		}

		void LateUpdate() {
			if (mainCam != null) {
				var c = mainCam.backgroundColor;
				const float factor = .25f;
				bg.backgroundColor = new Color(c.r*factor,c.g*factor,c.b*factor,1);
			}
			float alpha = 1-Tween.EaseOut((sceneTempo-1)*1.33f+1);
			if (Mathf.Approximately(alpha,0)) {
				fade.enabled = false;
			} else {
				fade.enabled = true;
				var c = fade.material.color;
				fade.material.color = new Color(c.r,c.g,c.b,alpha);
			}
		}

		public static void ChangeScene(int index) {
			me.goToScene = index;
		}
	}
}