﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	[RequireComponent(typeof(BoxCollider2D))]
	public class HudRegion:MonoBehaviour {
		Transform tr;
		BoxCollider2D col;

		void Awake() {
			tr = transform;
			col = GetComponent<BoxCollider2D>();
		}

		void Update() {
			if (!Level.me.mouseFree || !Level.me.mousePresent) return;
			var tp = tr.position;
			var ts = tr.lossyScale;
			var x = tp.x+col.offset.x*ts.x;
			var y = tp.y+col.offset.y*ts.y;
			var w = col.size.x*ts.x*.5f;
			var h = col.size.y*ts.y*.5f;
			if (Mathf.Approximately(w,0) || Mathf.Approximately(h,0)) return;
			var pos = Level.me.mouseHud;
			if (pos.x >= x-w && pos.x <= x+w && pos.y >= y-h && pos.y <= y+h) {
				Level.me.mouseFree = false;
			}
		}
	}
}