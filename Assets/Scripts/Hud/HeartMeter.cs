﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class HeartMeter:MonoBehaviour {
		Transform tr;
		Renderer[] hearts;

		int value = 0;
		Vector3 pos;

		float shake = 0;

		void Awake() {
			tr = transform;
			hearts = new Renderer[Hud.maxHealth];
			for (int a = 0; a < Hud.maxHealth; a++) {
				hearts[a] = tr.Find((Hud.maxHealth-a-1).ToString()).GetComponent<Renderer>();
			}
		}

		void Start() {
			pos = tr.localPosition;
			value = Level.me.hud.health;
			UpdateHearts(0);
		}

		void Update() {
			if (value != Level.me.hud.health) {
				int d = Level.me.hud.health-value;
				value = Level.me.hud.health;
				UpdateHearts(d);
			}
			if (shake > 0) {
				shake -= Time.deltaTime;
				if (shake <= 0) {
					shake = 0;
					tr.localPosition = pos;
				} else {
					tr.localPosition = new Vector3(pos.x+Mathf.Sin(Time.time*50)*shake*.1f,pos.y,pos.z);
				}
			}
		}

		void UpdateHearts(int d) {
			for (int a = 0; a < Hud.maxHealth; a++) {
				hearts[a].material.mainTextureOffset = new Vector2((a < value) ? 0 : .5f,0);
			}
			if (d < 0) shake = 1;
		}
	}
}