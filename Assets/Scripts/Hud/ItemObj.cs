﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class ItemObj:MonoBehaviour {
		public Transform tr { get; private set; }
		public Renderer rend { get; private set; }
		public bool select = false;

		public ItemOrigin origin = ItemOrigin.Inventory;
		public int position = 0;

		bool firstType = true;
		ItemType m_type;
		public ItemType type {
			get {
				return m_type;
			}
			set {
				if (!firstType && m_type == value) return;
				firstType = false;
				m_type = value;
				UpdateType();
			}
		}
		bool firstQuantity = true;
		int m_quantity = 1;
		public int quantity {
			get {
				return m_quantity;
			}
			set {
				if (!firstQuantity && m_quantity == value) return;
				firstQuantity = false;
				m_quantity = value;
				UpdateQuantity();
			}
		}
		
		Renderer bg;
		TextMesh text;

		void Awake() {
			tr = transform;
			rend = tr.Find("sprite").GetComponent<Renderer>();
			Transform t;
			t = tr.Find("textbg");
			if (t != null) bg = t.GetComponent<Renderer>();
			t = tr.Find("text");
			if (t != null) text = t.GetComponent<TextMesh>();
		}

		public bool PosInside(Vector2 pos) {
			var p = tr.position;
			const float r = .4f;
			return pos.x >= p.x-r && pos.x <= p.x+r && pos.y >= p.y-r && pos.y <= p.y+r;
		}

		void UpdateType() {
			rend.material.mainTexture = Resources.Load<Texture2D>("hud/item/"+type.ToString().ToLower());
		}
		
		void UpdateQuantity() {
			if (quantity > 1) {
				rend.enabled = true;
				if (bg != null) bg.enabled = true;
				if (text != null) text.text = quantity.ToString();
			} else if (quantity > 0) {
				rend.enabled = true;
				if (bg != null) bg.enabled = false;
				if (text != null) text.text = string.Empty;
			} else {
				rend.enabled = false;
				if (bg != null) bg.enabled = false;
				if (text != null) text.text = string.Empty;
			}
		}

		static GameObject prefab;
		public static ItemObj Create(Transform parent,ItemType type,int quantity = 1) {
			if (prefab == null) {
				prefab = Resources.Load<GameObject>("prefabs/item");
			}
			var item = Instantiate(prefab).GetComponent<ItemObj>();
			item.tr.parent = parent;
			item.type = type;
			item.quantity = quantity;
			return item;
		}
		public static ItemObj Create(ItemType type,int quantity = 1) {
			return Create(Level.me.hud.tr,type,quantity);
		}
		public static ItemObj Create(Transform parent) {
			return Create(parent,ItemType.Axe,0);
		}
		public static ItemObj Create() {
			return Create(Level.me.hud.tr);
		}

		public static int MaxStack(ItemType type) {
			switch (type) {
				case ItemType.Sword:
				case ItemType.Axe:
				case ItemType.Hoe:
				case ItemType.Bucket:
				case ItemType.Bow: return 1;
				case ItemType.Fence:
				case ItemType.Campfire: return 8;
				default: return 32;
			}
		}
	}

	public enum ItemType {
		Sword,
		Axe,
		Hoe,
		CarrotSeed,
		Carrot,
		RawBeef,
		RoastBeef,
		Wood,
		Bucket,
		FilledBucket,
		Fence,
		Web,
		Bow,
		Arrow,
		Campfire,
		Sapling,
	}

	public enum ItemOrigin {
		Inventory,
		Crafting,
		Storage,
	}
}