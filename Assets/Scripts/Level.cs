﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Terrinha {
	public class Level:MonoBehaviour {
		public static Level me { get; private set; }

		public Transform tr { get; private set; }
		public Transform worldTr { get; private set; }
		public Transform homeTr { get; private set; }

		public Player player { get; private set; }
		public Tripod tripod { get; private set; }

		public Hud hud { get; private set; }

		public const float worldRadius = 15;
		public const float gravity = 10;

		public float time { get; private set; }
		public const float dayCycleDuration = 4*60;

		public bool mousePresent { get; private set; }
		public Vector2 mouseWorld { get; private set; }
		public float mouseRadius { get; private set; }
		public float mouseAngle { get; private set; }
		public Vector2 mouseHud { get; private set; }
		public bool mouseFree;

		public const float playerActionRadius = 2;
		float playerCooldown = 0;

		List<SpawnProperties> spawnProperties;
		HashSet<int> spawnAllowedPositions;
		HashSet<int> spawnPreferredPositions;

		public FixedEntity[] fixedEntities { get; private set; }

		public List<Arrow> arrows { get; private set; }

		Transform particlesTr;
		ParticleSystem particles;

		public const int fixedLength = 95;

		void Awake() {
			Game.Verify();
			me = this;

			tr = transform;
			worldTr = tr.Find("world");
			homeTr = tr.Find("home");
			worldTr.localScale = new Vector3(worldRadius*2,worldRadius*2,1);

			player = tr.Find("player").GetComponent<Player>();
			tripod = tr.Find("tripod").GetComponent<Tripod>();

			hud = tr.Find("hud").GetComponent<Hud>();

			time = 0;

			mousePresent = false;

			spawnProperties = new List<SpawnProperties> {
				new SpawnProperties("cow",20,5,new SpawnExtras {
					mode = SpawnMode.Day,
					avoidPlayer = true,
					avoidCampfires = true,
					avoidPlantations = true,
				}),
				new SpawnProperties("spider",20,5,new SpawnExtras {
					mode = SpawnMode.Night,
					avoidPlayer = true,
					avoidCampfires = true,
					avoidPlantations = false,
				}),
				new SpawnProperties("cloud",8,3,new SpawnExtras {
					mode = SpawnMode.AllTime,
					avoidPlayer = false,
					avoidCampfires = false,
					avoidPlantations = false,
				}),
			};
			spawnAllowedPositions = new HashSet<int>();
			spawnPreferredPositions = new HashSet<int>();
			
			var homeFixedEntity = homeTr.gameObject.AddComponent<FixedEntity>();
			fixedEntities = new FixedEntity[fixedLength];
			for (int a = 93; a < fixedLength; a++) {
				fixedEntities[a] = homeFixedEntity;
			}
			for (int a = 0; a <= 2; a++) {
				fixedEntities[a] = homeFixedEntity;
			}

			arrows = new List<Arrow>();

			particlesTr = tr.Find("particles");
			particles = particlesTr.GetComponent<ParticleSystem>();
		}

		void Start() {
			var tree = PlaceEntity("tree",fixedLength-6).GetComponent<TreeObj>();
			tree.grown = true;
			tripod.FocusAt(player.entity.tr.position,1,true);
		}

		void Update() {
			time = (time+Time.deltaTime) % dayCycleDuration;

			Vector2 mWorld,mHud;
			if (Input.GetMouseAt(tripod.cam,out mWorld) && Input.GetMouseAt(hud.cam,out mHud)) {
				Cursor.visible = false;
				mousePresent = true;
				mouseWorld = mWorld;
				mouseRadius = mWorld.magnitude;
				mouseAngle = Mathf.Atan2(-mWorld.x,mWorld.y);
				mouseHud = mHud;
			} else {
				Cursor.visible = true;
				mousePresent = false;
			}
			
			if (hud.health > 0) SpawnEntities();

			if (hud.health <= 0) {
				tripod.visibleSize = 10;
			} else if (hud.homeMenuOpen != HudHomeMenu.None) {
				tripod.visibleSize = 5;
			} else {
				tripod.visibleSize = 7;
			}
			tripod.FocusAt(player.entity.tr.position,2);
			if (hud.health <= 0) {
				mouseFree = false;
			} else if (playerCooldown > 0) {
				mouseFree = false;
				playerCooldown -= Time.deltaTime*2;
				if (playerCooldown <= 0) playerCooldown = 0;
			} else {
				mouseFree = true;
			}
		}

		public bool DetectPlayerClick(Vector2 pos,float radius) {
			if (!mouseFree || !Input.mousePress) return false;
			var playerPos = (Vector2)player.entity.tr.position;
			if ((pos-mouseWorld).sqrMagnitude > radius*radius) return false;
			if ((mouseWorld-playerPos).sqrMagnitude > playerActionRadius*playerActionRadius) return false;
			return true;
		}

		public void ConsumePlayerClick() {
			mouseFree = false;
		}

		public void ActivatePlayerCooldown() {
			playerCooldown = 1;
			player.ItemAction();
		}

		public float GetPlayerAttackDamage() {
			var i = hud.selectedItem;
			if (i == null) return 1;
			switch (i.type) {
				case ItemType.Sword: return 3;
				case ItemType.Axe: return 2;
				case ItemType.Hoe: return 1.5f;
				default: return 1;
			}
		}

		public const float arrowAttackDamage = 2.5f;

		public float GetPlayerTreeChopDamage() {
			var i = hud.selectedItem;
			return (i != null && i.type == ItemType.Axe) ? .34f : .11f;
		}

		public bool PlaceEntity() {
			if (hud.health <= 0) return false;
			int pos = GetFixedUnit(mouseAngle);
			var fe = fixedEntities[pos];
			switch (hud.selectedItem.type) {
				case ItemType.Campfire:
					if (fe != null) return fe.type == FixedEntityType.Campfire;
					PlaceEntity("campfire",pos);
					player.PlaySfx(player.sfxAction);
					break;
				case ItemType.Sapling:
					if (fe != null) return fe.type == FixedEntityType.Tree;
					PlaceEntity("tree",pos);
					player.PlaySfx(player.sfxAction);
					break;
				case ItemType.Hoe:
					if (fe != null) return fe.type == FixedEntityType.Plantation;
					PlaceEntity("plantation",pos);
					player.PlaySfx(player.sfxAction);
					break;
			}
			if (hud.selectedItem.type != ItemType.Hoe) {
				hud.selectedItem.quantity--;
				if (hud.selectedItem.quantity <= 0) {
					Destroy(hud.selectedItem.gameObject);
					hud.selectedItem = null;
				}
			}
			return true;
		}

		FixedEntity PlaceEntity(string prefabPath,int pos) {
			var obj = Instantiate(Resources.Load<GameObject>("prefabs/"+prefabPath)).GetComponent<FixedEntity>();
			obj.SetFixedAtUnit(pos);
			return obj;
		}

		public void FireArrow(float fromRadius,float fromAngle,float toRadius,float toAngle) {
			float dir = toAngle-fromAngle;
			while (dir < -Mathf.PI) dir += Mathf.PI*2;
			while (dir > Mathf.PI) dir -= Mathf.PI*2;
			var arrow = Instantiate(Resources.Load<GameObject>("prefabs/arrow")).GetComponent<Arrow>();
			arrow.entity.dir = Mathf.Sign(dir);
			arrow.entity.velocity = Mathf.Abs(dir)*worldRadius*1.5f;
			arrow.entity.dirLerp = 100/Time.deltaTime;
			arrow.entity.jumpForce = Mathf.Sqrt(Mathf.Max(0,toRadius-fromRadius))*5;
			arrow.entity.angle = fromAngle;
			arrow.entity.height = fromRadius;
			arrow.entity.Jump();
		}

		public bool DetectArrow(Vector2 pos,float radius,out float angle) {
			for (int a = 0; a < arrows.Count; a++) {
				if (((Vector2)arrows[a].entity.tr.position-pos).sqrMagnitude <= radius*radius) {
					angle = arrows[a].entity.angle;
					arrows[a].Destroy();
					return true;
				}
			}
			angle = 0;
			return false;
		}

		public void Dust(Vector2 pos,int count = 30) {
			particlesTr.position = new Vector3(pos.x,pos.y,particlesTr.position.z);
			particles.Emit(count);
		}

		public static int GetFixedUnit(float angle) {
			angle = (angle*fixedLength)/(2*Mathf.PI);
			while (angle < 0) angle += fixedLength;
			return Mathf.FloorToInt(angle)%fixedLength;
		}

		public static float GetAngleFromFixedUnit(int pos) {
			float angle = (pos*2*Mathf.PI)/fixedLength;
			if (angle >= Mathf.PI) angle -= Mathf.PI*2;
			return angle;
		}

		void SpawnEntities() {
			float angleNightStart = time*Mathf.PI*2/dayCycleDuration;
			float angleNightEnd = angleNightStart+Mathf.PI;
			float angleDayStart = angleNightEnd;
			float angleDayEnd = angleDayStart+Mathf.PI;
			const float dontSpawn = .15f*Mathf.PI;
			angleDayStart += dontSpawn;
			angleDayEnd -= dontSpawn;
			angleNightStart += dontSpawn;
			angleNightEnd -= dontSpawn;
			int dayStart = GetFixedUnit(angleDayStart);
			int dayEnd = GetFixedUnit(angleDayEnd);
			int nightStart = GetFixedUnit(angleNightStart);
			int nightEnd = GetFixedUnit(angleNightEnd);
			int playerStart = GetFixedUnit(player.entity.angle-Tripod.visibleRange);
			int playerEnd = GetFixedUnit(player.entity.angle+Tripod.visibleRange);
			
			for (int a = 0; a < spawnProperties.Count; a++) {
				var p = spawnProperties[a];
				p.tempo -= Time.deltaTime;
				if (p.tempo <= 0) {
					if (p.count < p.max) {
						p.tempo = p.duration;
						int start,end;
						if (p.extras.mode == SpawnMode.AllTime) {
							start = 0;
							end = fixedLength;
						} else {
							start = (p.extras.mode == SpawnMode.Day) ? dayStart : nightStart;
							end = (p.extras.mode == SpawnMode.Day) ? dayEnd : nightEnd;
						}
						if (start > end) {
							for (int i = start; i < fixedLength; i++) {
								spawnAllowedPositions.Add(i);
								spawnPreferredPositions.Add(i);
							}
							for (int i = 0; i <= end; i++) {
								spawnAllowedPositions.Add(i);
								spawnPreferredPositions.Add(i);
							}
						} else {
							for (int i = start; i <= end; i++) {
								spawnAllowedPositions.Add(i);
								spawnPreferredPositions.Add(i);
							}
						}
						if (p.extras.avoidPlayer) {
							if (playerStart > playerEnd) {
								for (int i = playerStart; i < fixedLength; i++) {
									spawnAllowedPositions.Remove(i);
									spawnPreferredPositions.Remove(i);
								}
								for (int i = 0; i <= playerEnd; i++) {
									spawnAllowedPositions.Remove(i);
									spawnPreferredPositions.Remove(i);
								}
							} else {
								for (int i = playerStart; i <= playerEnd; i++) {
									spawnAllowedPositions.Remove(i);
									spawnPreferredPositions.Remove(i);
								}
							}
						}
						if (p.extras.avoidCampfires) {
							for (int i = 0; i < fixedLength; i++) {
								if (fixedEntities[i] != null && fixedEntities[i].type == FixedEntityType.Campfire) {
									for (int j = i-3; j <= i+3; j++) {
										int k;
										if (j < 0) {
											k = j+fixedLength;
										} else if (j >= fixedLength) {
											k = j-fixedLength;
										} else {
											k = j;
										}
										spawnPreferredPositions.Remove(k);
									}
								}
							}
						}
						if (p.extras.avoidPlantations) {
							for (int i = 0; i < fixedLength; i++) {
								if (fixedEntities[i] != null && fixedEntities[i].type == FixedEntityType.Plantation) {
									spawnPreferredPositions.Remove(i);
								}
							}
						}
						var set = spawnPreferredPositions;
						if (set.Count == 0) set = spawnAllowedPositions;
						if (set.Count == 0) continue;
						int r = Random.Range(0,set.Count);
						int count = 0;
						float angle = 0;
						foreach (var n in set) {
							if (count == r) {
								angle = GetAngleFromFixedUnit(n);
								break;
							}
							count++;
						}
						spawnAllowedPositions.Clear();
						spawnPreferredPositions.Clear();
						Instantiate(p.prefab).GetComponent<Spawnable>().SetSpawn(p,tr,angle);
					} else {
						p.tempo = Random.Range(0,p.duration);
					}
				}
			}
		}
		
		public class SpawnProperties {
			public GameObject prefab;
			public float duration,tempo;
			public int count,max;
			public SpawnExtras extras;

			public SpawnProperties(string prefabPath,int perDay,int max,SpawnExtras extras) {
				prefab = Resources.Load<GameObject>("prefabs/"+prefabPath);
				duration = dayCycleDuration/perDay;
				tempo = Random.Range(duration,duration*2);
				this.extras = extras;
				count = 0;
				this.max = max;
			}
		}

		public struct SpawnExtras {
			public SpawnMode mode;
			public bool avoidPlayer;
			public bool avoidCampfires;
			public bool avoidPlantations;
		}

		public enum SpawnMode {
			Day,
			Night,
			AllTime,
		}
	}
}